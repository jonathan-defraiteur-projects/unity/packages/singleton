using UnityEngine;

namespace JonathanDefraiteur.Runtime
{
    public abstract class LazyLoadedSingletonBehaviour<T> : SingletonBehaviour<T> where T : MonoBehaviour
    {
        public new static T Instance
        {
            get {
                if (_applicationIsQuitting) {
                    Debug.LogWarning("[Singleton] Instance '" + typeof(T) +
                                     "' already destroyed on application quit." +
                                     " Won't create again - returning null.");
                    return null;
                }

                lock (_lock) {
                    if (null != _instance) {
                        return _instance;
                    }
                    
                    T[] objs = FindObjectsOfType<T>();
                    if (0 == objs.Length) {
                        _instance = LazyLoadInstance();
                    }
                    else if (1 == objs.Length) {
                        _instance = objs[0];
                    }
                    else {
                        Debug.LogError("[Singleton] You must have at most one " + typeof(T).Name + " in the scene.");
                    }

                    return _instance;
                }
            }
        }

        private static T LazyLoadInstance()
        {
            var obj = new GameObject();
            var instance = obj.AddComponent<T>();
            instance.name = typeof(T).Name;
            return instance;
        }
    }
}
