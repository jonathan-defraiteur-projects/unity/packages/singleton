using UnityEngine;

namespace JonathanDefraiteur.Runtime
{
    public abstract class PersistentSingletonBehaviour<T> : SingletonBehaviour<T> where T : MonoBehaviour
    {
        public new static T Instance
        {
            get {
                if (_applicationIsQuitting) {
                    Debug.LogWarning("[Singleton] Instance '" + typeof(T) +
                                     "' already destroyed on application quit." +
                                     " Won't create again - returning null.");
                    return null;
                }

                lock (_lock) {
                    if (null != _instance) {
                        return _instance;
                    }
                    
                    T[] objs = FindObjectsOfType<T>();
                    if (objs.Length == 1) {
                        _instance = objs[0];
                        DontDestroyOnLoad(_instance.gameObject);
                    }
                    else {
                        Debug.LogError("[Singleton] You must have at most one " + typeof(T).Name + " in the scene.");
                    }

                    return _instance;
                }
            }
        }

        protected new bool SelfRegisterInstance()
        {
            if (base.SelfRegisterInstance()) {
                DontDestroyOnLoad(this.gameObject);
                return true;
            }

            return false;
        }
    }
}