using UnityEngine;

namespace JonathanDefraiteur.Runtime
{
    public abstract class LazyLoadedSingletonScriptable<T> : SingletonScriptable<T> where T : ScriptableObject
    {

        public new static T Instance
        {
            get {
                if (null != _instance) {
                    return _instance;
                }

                lock (_lock) {
                    T[] objs = Resources.FindObjectsOfTypeAll<T>();
                    if (0 == objs.Length) {
                        _instance = LazyLoadInstance();
                    }
                    else if (1 == objs.Length) {
                        _instance = objs[0];
                    }
                    else {
                        Debug.LogError("[Singleton] You must have at most one " + typeof(T).Name + " in the Resources folder.");
                    }
                    
                    return _instance;
                }
            }
        }

        private static T LazyLoadInstance()
        {
            return CreateInstance<T>();
        }
    }
}