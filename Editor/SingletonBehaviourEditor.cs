﻿using JonathanDefraiteur.Runtime;

namespace JonathanDefraiteur.Editor
{
    [UnityEditor.CustomEditor(typeof(SingletonBehaviour), true)]
    public class SingletonBehaviourEditor : AbstractSingletonEditor
    { }
}
